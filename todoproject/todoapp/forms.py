from django.forms import Form
from django.forms import CharField


class AddTodoItemForm(Form):
    """
    Форма добавления элемента списка дел.
    """
    content = CharField(label='')
