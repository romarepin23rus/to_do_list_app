from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import View

from .forms import AddTodoItemForm
from .models import TodoListItem


class ToDoAppView(LoginRequiredMixin, View):
    """
    Отображение списка дел пользователю.
    """
    http_method_names = ['get']

    def get(self, request):
        all_todo_items = TodoListItem.objects.filter(user_id=request.user.id).all()
        return render(request, 'todolist.html', {'form': AddTodoItemForm, 'all_items': all_todo_items})


class AddTodoItem(LoginRequiredMixin, View):
    """
    Добавление нового пункта в список дел.
    """
    http_method_names = ['post']

    def post(self, request):
        form = AddTodoItemForm(request.POST)
        if form.is_valid():
            new_item = TodoListItem(user_id=request.user.id, content=form.cleaned_data['content'])
            new_item.save()
        return HttpResponseRedirect('/todoapp/')


class CrossOutTodoItem(LoginRequiredMixin, View):
    """
    Вычёркивание пункта в списке дел.
    """
    http_method_names = ['post']

    def post(self, request, todo_item_index: int):
        todo_item = TodoListItem.objects.get(id=todo_item_index)
        todo_item.is_crossed_out = True
        todo_item.save()
        return HttpResponseRedirect('/todoapp/')


class DeleteTodoItem(LoginRequiredMixin, View):
    """
    Удаление пункта из списка дел.
    """
    http_method_names = ['post']

    def post(self, request, todo_item_index: int):
        todo_item = TodoListItem.objects.get(id=todo_item_index)
        todo_item.delete()
        return HttpResponseRedirect('/todoapp/')


class RedirectToMainView(LoginRequiredMixin, View):
    """
    Перенаправление на главную страницу.
    """
    http_method_names = ['get']

    def get(self, request):
        return HttpResponseRedirect('/todoapp/')
