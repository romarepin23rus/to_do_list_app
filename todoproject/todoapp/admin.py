from django.contrib.admin import ModelAdmin
from django.contrib.admin import register
from django.contrib.admin import site

from .models import TodoListItem


# переопределение внешнего вида административной панели
site.site_header = 'Административная панель'
site.site_title = 'Мой список дел'
site.index_title = 'Управление данными'


class CustomModelAdmin(ModelAdmin):
    """
    Общие настройки кастомизированной таблицы.
    """
    save_on_top = True


@register(TodoListItem)
class TodoListItemAdmin(CustomModelAdmin):
    """
    Таблица "Элементы списка дел"
    """
    list_display = (
        'id',
        'user',
    )
    search_fields = (
        'content',
    )
