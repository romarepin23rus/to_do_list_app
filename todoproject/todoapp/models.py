from django.conf.global_settings import AUTH_USER_MODEL
from django.db.models import CASCADE
from django.db.models import AutoField
from django.db.models import BooleanField
from django.db.models import ForeignKey
from django.db.models import Model
from django.db.models import TextField


class TodoListItem(Model):
    """
    Элемент списка дел.
    """
    id = AutoField(primary_key=True)
    user = ForeignKey(AUTH_USER_MODEL, on_delete=CASCADE, verbose_name='Пользователь')
    is_crossed_out = BooleanField(default=False, verbose_name='Вычеркнуто')
    content = TextField(verbose_name='Содержание')

    class Meta:
        verbose_name = 'Элемент списка дел'
        verbose_name_plural = 'Элементы списка дел'
