from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import View

from .forms import UserRegisterForm


class UserRegister(View):
    """
    Регистрация пользователей.
    """
    http_method_names = ['get', 'post']

    def get(self, request):
        if not request.user.is_authenticated:
            form = UserRegisterForm()
            return render(request, 'register.html', {'form': form})
        else:
            return HttpResponseRedirect('/todoapp/')

    def post(self, request):
        if not request.user.is_authenticated:
            form = UserRegisterForm(request.POST)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect('/todoapp/')
            else:
                return render(request, 'register.html', {'form': form})
        else:
            return HttpResponseRedirect('/todoapp/')
