from django.contrib.admin import site
from django.contrib.auth.views import LoginView
from django.contrib.auth.views import LogoutView
from django.urls import path

from todoapp.views import AddTodoItem
from todoapp.views import CrossOutTodoItem
from todoapp.views import DeleteTodoItem
from todoapp.views import RedirectToMainView
from todoapp.views import ToDoAppView
from users.views import UserRegister

urlpatterns = [
    path('', RedirectToMainView.as_view(), name='redirect_to_main'),
    path('register/', UserRegister.as_view(), name='register'),
    path('login/', LoginView.as_view(template_name='login.html', redirect_authenticated_user=True), name='login'),
    path('logout/', LogoutView.as_view(template_name='logout.html'), name='logout'),
    path('add_todo_item/', AddTodoItem.as_view(), name='add_todo_item'),
    path('admin/', site.urls),
    path('cross_out_todo_item/<int:todo_item_index>/', CrossOutTodoItem.as_view(), name='cross_out_todo_item'),
    path('delete_todo_item/<int:todo_item_index>/', DeleteTodoItem.as_view(), name='delete_todo_item'),
    path('todoapp/', ToDoAppView.as_view(), name='todo_list'),
]
